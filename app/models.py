from django.db import models
from django.db.models.deletion import PROTECT
from django.contrib.auth.models import User

# Create your models here.
class Partido(models.Model):
    nombre = models.CharField(max_length=45, null=False)
    visitas = models.IntegerField(null= True)
    # Creador = models.ForeignKey(
    #     User,
    #     related_name='partidos',
    #     null=False,
    #     on_delete=models.PROTECT
    # )
    class Meta:
        app_label: 'app'



class Individuo(models.Model):
    nombre = models.CharField(max_length=45, null=False)
    apellido = models.CharField(max_length=45, null=False)
    fecha_nacimiento = models.DateField(null= True)
    Aprobado = models.BooleanField(null= True)
    Visitas = models.IntegerField(null= True)
    # Creador = models.ForeignKey(
    #     User,
    #     related_name='individuos',
    #     null=False,
    #     on_delete=models.PROTECT
    # )
    class Meta:
        app_label: 'app'

class Afiliacion(models.Model):
    Id_Individuo = models.ForeignKey(
        Individuo,
        related_name='afiliaciones',
        null=False,
        on_delete=models.PROTECT
    )
    Id_Partido = models.ForeignKey(
        Partido,
        related_name='afiliaciones',
        null=False,
        on_delete=models.PROTECT
    )
    Fecha_Ingreso = models.DateField(null=False)
    Fecha_Salida = models.DateField(null=False)
    Aprobado = models.BooleanField
    class Meta:
        app_label: 'app'

class Proceso(models.Model):
    Titulo = models.CharField(max_length=45, null=False)
    Fecha_Inicio = models.DateField(null=False)
    Fecha_Fin = models.DateField(null=True)
    Abierto = models.BooleanField(null=False)
    Entidad = models.CharField(max_length=45, null=False)
    Monto = models.IntegerField(null=True)
    Comentarios = models.CharField(max_length=45, null=True)
    Aprobado = models.BooleanField(null=True)
    Visitas = models.IntegerField(null=True)
    # Creador = models.ForeignKey(
    #     User,
    #     related_name='afiliaciones',
    #     null=False,
    #     on_delete=models.PROTECT
    # )
    class Meta:
        app_label: 'app'
        


class Implicado(models.Model):
    Id_Afiliado = models.ForeignKey(
        Afiliacion,
        related_name='Implicados',
        null=False,
        on_delete=models.PROTECT
    )
    Id_Proceso = models.ForeignKey(
        Proceso,
        related_name='implicados',
        null=False,
        on_delete=models.PROTECT
    )
    Fecha = models.DateField(null=True)
    Acusacion= models.CharField(max_length=45, null=True)
    Culpable = models.BooleanField(null=True)
    Pena = models.CharField(max_length=45, null=True)
    Comentarios = models.CharField(max_length=45, null=True)
    class Meta:
        app_label: 'app'