# Generated by Django 3.2.8 on 2021-10-22 11:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20211022_0320'),
    ]

    operations = [
        migrations.RenameField(
            model_name='individuo',
            old_name='Apellido',
            new_name='apellido',
        ),
        migrations.RenameField(
            model_name='individuo',
            old_name='Nombre',
            new_name='nombre',
        ),
        migrations.RemoveField(
            model_name='individuo',
            name='Creador',
        ),
        migrations.AddField(
            model_name='individuo',
            name='Aprobado',
            field=models.BooleanField(null=True),
        ),
        migrations.AddField(
            model_name='individuo',
            name='fecha_nacimiento',
            field=models.DateField(null=True),
        ),
    ]
