from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .models import Partido, Individuo, Proceso

# Create your views here.

def login_view(request):
    return render(request, 'app/registro.html')

def login_post(request):

    u = request.POST['username']
    p = request.POST['password']

    usuario = authenticate(username=u, password=p)

    if usuario is None:
        return render (request, 'app/error.html')
    else: 
        # login(request, usuario)
      return redirect('app:index')

def cerrar_sesion(request):
    logout(request)
    return redirect ('app:registro')

def form_registro_view(request):
    return render(request, 'app/registro.html')

def form_registro_post(request):
    nombre = request.POST['nombre']
    apellido = request.POST['apellido']
    username = request.POST['username']
    password = request.POST['password']

    usuario = User()
    usuario.first_name = nombre
    usuario.last_name = apellido
    usuario.username = username
    usuario.set_password(password)

    usuario.save()

    return redirect ('app:registro')

def form_crear_partido_view(request):
    return render(request, 'app/CrearPartido.html')

def crear_partido_post(request):

    nombre = request.POST['nombrepartido']
    p = Partido()
    p.nombre = nombre
    p.save()
    return redirect('app:consultar-lista-partidos')

def form_crear_individuo_view(request):
    return render(request, 'app/CrearIndiv.html')

def crear_individuo_post(request):
    nombre= request.POST['nombreindividuo']
    apellido = request.POST['apellidoindividuo']
    fecha = request.POST['fechaindividuo']
    i = Individuo()
    i.nombre = nombre
    i.apellido = apellido
    i.fecha_nacimiento = fecha
    i.save()
    return redirect('app:consultar-lista-individuos')

def index(request):
    return render(request, 'app/index.html')

def consultarindividuo_view(request):
    return render(request, 'app/ConsultarIndividuo.html')

def consultarlistaindividuos_view(request):

    lista_apellidos = Individuo.objects.order_by('apellido')
    contexto = {
            'individuos' : lista_apellidos
            
        }

    return render(request, 'app/ConsultarListaIndividuos.html', contexto)

def consultarpartido_view(request):
    return render(request, 'app/ConsultarPartido.html')

def consultarlistapartidos_view(request):
    lista_partidos = Partido.objects.order_by('nombre')
    contexto = { 
        'partidos' : lista_partidos
     }
    return render(request, 'app/ConsultarListaPartidos.html',contexto)

def consultarproceso_view(request):
    return render(request, 'app/ConsultarProceso.html')


def afiliarindividuo_view(request):
    return render(request, 'app/AfiliarIndiv.html')

def configuracion_view(request):
    return render(request, 'app/configuracion.html')

def crearindividuo_view(request):
    return render(request, 'app/CrearIndiv.html')

def implicarindividuo_view(request):
    return render(request, 'app/ImplicarIndiv.html')

def homeadministrador_view(request):
    return render(request, 'app/index2.html')

def registro_view(request):
    return render(request, 'app/registro.html')
    


def crearpartido_view(request):
    return render(request, 'app/CrearPartido.html')

def aprobarafiliacion_view(request):
    return render(request, 'app/AprobarAfiliacion.html')

def aprobarindividuo_view(request):
    return render(request, 'app/AprobarIndividuo.html')

def aprobarproceso_view(request):
    return render(request, 'app/AprobarProceso.html')

def habdesciudadano_view(request):
    return render(request, 'app/HabDesCiudadano.html')