from django.urls import path
from.import views

app_name = 'app'
urlpatterns = [
    path('home',views.index, name='index'),
    path('consultar-individuo/',views.consultarindividuo_view, name='consultar-individuo'),
    path('consultar-lista-individuos/',views.consultarlistaindividuos_view, name='consultar-lista-individuos'),
    path('consultar-partido/',views.consultarpartido_view, name='consultar-partido'),
    path('consultar-lista-partidos/',views.consultarlistapartidos_view, name='consultar-lista-partidos'),
    path('consultar-proceso/',views.consultarproceso_view, name='consultar-proceso'),

 
    path('afiliar-individuo/',views.afiliarindividuo_view, name='afiliar-individuo'),
    path('configuracion/',views.configuracion_view, name='configuracion'),
    path('crear-individuo/',views.crearindividuo_view, name='crear-individuo'),
    path('implicar-individuo/',views.implicarindividuo_view, name='implicar-individuo'),
    path('home-administrador/',views.homeadministrador_view, name='home-administrador'),
    path('',views.registro_view, name='registro'),

    path('crear-partido/',views.crearpartido_view, name='crear-partido'),
    path('aprobar-afiliacion/',views.aprobarafiliacion_view, name='aprobar-afiliacion'),
    path('aprobar-individuo/',views.aprobarindividuo_view, name='aprobar-individuo'),
    path('aprobar-proceso/',views.aprobarproceso_view, name='aprobar-proceso'),
    path('hab-des-ciudadano/',views.habdesciudadano_view, name='hab-des-ciudadano'),

    # login
    path('login/',views.login_view, name='login'),
    path('login_post/',views.login_post, name='login_post'),

    path('logout/', views.cerrar_sesion, name='cerrar_sesion'),
    path('registro/',views.form_registro_view, name='form_registro_view'),
    path('registro_post/',views.form_registro_post, name='form_registro_post'),

        # crearpartido
    path('crear-partido/crear',views.form_crear_partido_view, name='form_crear_partido_view'),
    path('crear-partido/crear_post',views.crear_partido_post, name='crear_partido_post'),
        # crearindividuo
    path('crear-individuo/crear',views.form_crear_individuo_view, name='form_crear_individuo_view'),
    path('crear-individuo/crear_post',views.crear_individuo_post, name='crear_individuo_post'),   
]
